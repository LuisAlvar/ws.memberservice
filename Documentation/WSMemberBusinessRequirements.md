# Member Web Service - Business Requirements 
The role of a healthcare member will not be the end-user of this application, it will be an entity in which a small provider of healthcare services will render services to. Provider will collect 
medical insurance information along with personal information such SSN from a member. WorkSpace will need to 
analysis any HIPAA regulatories that (the "Software") will need to follow.

The goal of this document is not to discover all possible items for this 
core web service, but to estaiblish a scalarable core web service with a 
template of basic features first. 

# Documentation Version 

Date |  Authors |  Version |Description 
-----|----------|----------|------------
5/30/18 |  Luis Alvarez | v1.0 | Initial Document 

# Data Collection of Member

This section will mock the process in which the provider will interact with 
the member. Since, we currently do not have a client to consult with. 

Steps |  Phrase | Description 
------|---------|------
1 | Personal Information |  Members will fill a form with the following information <br> **Personal information:** <br>  - First Name & Last Name <br> - Street Number, Street Name, Street Type, City, State, Zip Code <br> - Gender <br> - Home Phone <br> - Cell Phone <br> - Email <br> **Personal Social Info:** <br> - Social Security <br>  **References** <br> - First Name <br> - Last Name <br> - Relationship To Member <br> - Phone Number <br>
2 | Medical History Information  | Member may need to provide a quick medical history <br> **Quick Medical Check** <br> - Last Check-Up Date <br> **Medical Conditions** <br>
3 | Gender Information | Member can enter how they wish to identify as 
4 | Medical Insurance Information  | Member will need to provide medical insurance information <br> **Medical Provider** <br> - Provider Name - <br> - Provider Main Point of Contact Phone Number <br> - Member ID & Group ID  <br>
5 | Medical Authorization Information | Member may need to provide Authorization documents to the provider to confirm appointment <br> **Information on Primary Doctor's Note** <br> 
6 | Member History With Provider | Provider will keep track of member's past, current, and future appointments  

The phrases above will serve merely as a template - as a starting point. 
The goal is to make a scalarable core web service that we can easily add more features.

# User Cases 

These User Cases represent the functional features the main application may use related to this core web service. The main application will have exclusive access to this web service in the background. 

Case No | Call From | Case Description 
--------|-----------|-----------------
1   | Main Application | **Daily Display of Member** <br> Display all members on the current Date or Week that provider will see 
2 | Main Application | **Search For A Member** <br> Search for a particular member 
3 | Main Application | **Add a new Member** <br> Add a new member starting with personal information and medical information, and also set up medical history and authorization information if required.
4| Main Application | **Display Full Profile of Member** <br> Show all personal/medical/documentation on the current member
5| Main Application | **Update Member Information** <br> Update both personal/medical/documentation information 
6| Main Application | **Deactive A Member** <br> Deactive a member that has not visit the provider in a whole tax year. 

# Technology Stack 

This is an initial analysis of the technology stack, which these web services will use independent of the main application techology stack. 

Technology | Options/Tech | Description 
-----------|--------------|-------------
.NetCore   |  Node.js     | We need a web service that may need to handle multi-threading, multi-tasking, or even parallel computation. .NetCore3.0 has the capacity to these features and can perform at an enterprise-level. Node.js is a still a good option but requires a lot of third-party libraries. 
Docker      |  Containerization of web services | With each web service we willl containerize it with a means of storage and its dependencies. 
Kubernetes | Container orchestration | "Enabling application to be release and update in an easy and fast way without downtime" - Kubernetes.io. Kubernetes will be used to cluster related web services. Its a containerization management platform without the benefit of auto-scalcing. Automates continer deployment, container (de)scaling & continer load balancing. It will manage multiple docker container. 
Data Storage | SQL Server vs Postgres | SQL Server docker image is vastly big; hence, there will be two relational-database servers depending on an environment. SQL Server will be for only production environments and Postgress will be used for dev/testing environments. 
Azure | Deployment | Azure is the main cloud platform for all web services
Azure Information Protection Services | Authenication and Authorization to web service |  Only an authenticated and authorized user can access these services for testing and main application can access it. 
Azure Pipeline | Continuous Deployment | The pipeline will be deploy to two different environments 
Pivotaltracker | Agile Software Development | For project management field 
SonaQ          | Code Analysis              | For static analysis of web service, meaning it compiles the web service and detect any vulnerabilities the web service may expose. 
Swagger | API Documentation | "The evolution of your API's functionality is inevitable, but the headache of maintaining API docs doesn't have to be."