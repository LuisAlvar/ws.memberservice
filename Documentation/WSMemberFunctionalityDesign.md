# Member Web Serivce - Functionality Design 
The role of a healthcare member will not be the end-user of this application. A member will be an entity in which a small provider of healthcare services will render services to. 
Provider will collect medical insurance information along with personal information such SSN from a member. WorkSpace will need to
analysis any HIPAA regulatories that the whole application (the "Software") will need to follow. 

#### Purpose 
The purpose of this internal document is to layout the foundation of this web service in terms of data architecture. 

# Documentation Version
This document may only be edit or revised by given LLC or Inc, procedures of the "Software", at the instruction of any senior official within company. 

Date |  Authors | Version | Description 
-----|----------|---------|-------------
5/31/2020 |  Luis Alvarez | v1.0 |  Initial Document Formation

# Data Member BreakDown 
In this section, we will analysis the actual data required from the member and how to organize such data.

# Service BreakDown 
Member service will only deal with personal information this includes the following tables: Member, Contact, Address, SocialSec, Reference, Gender, and Enrollment. 

Micro-services | Description 
--------------|-------------
Payment            | This service will be in charge on holding member payament information and charging members for only out-of-pock cost. 
MedicalInformation | This service will hold medical hisotry information and medical insurance information.

# Entity Relational Diagram on Member
This is the initial entity relational diagram of member and its closely related entities. I know this is not a traditional EER-diagram or following the EER development process.   

![Image of EER-Diagram](Images/Initial_EER_Diagram.png "EER-Diagram For Member")

# Data Dictionary 

##### Member 
Data Information | Description  | Table | Alias | Reference Table | Cardinality
-----------------|--------------|--------|------|-----------------|--------------
First Name | First name of member | Member | FirstName |
Middle Name | Middle name of member | Member | MiddleName|
Last Name | Las name of member  | Member | LastName |
Date of Birth | Date of Birth of Member | Member | DOB |
Gender | Gender of Member | Member | GenderId | Gender
Social Security | Social Security of Member | Member | SocialSecId | SocialSec

##### Gender 
These will be user-defined. These will be set by the Provider at anytime. 

Column | Description 
------|------------
GenderId |  Unique Identifier
GenderType |  Abbreviation 
GenderDesc | Gender Description 

##### SocialSec
Column | Description
-------|------------
SocialId    |  Unique Indentifier 
SocialKey   | Key 
SocialMask  | Mask 

##### PaymentType
Column | Description 
-------|------------
PayTypeId | Unique Identifier 
PaySetDate | The date that member gave this information
PayTermDate | The date the member termiate this information 
Active  | bool, if member cancels the payment method 
MemberId | Foreign key

##### CPay
Column | Description 
-------|------------
PaymentId | Unique Identifier 
PaymentNumberKey | The key of the payment key 
PaymentNumberMask | The mask payment number 
PaymentDate  |  The date on the card 
PaymentCVVKey  | The key of the paymentCVV 
PaymentCVVMask  | The mask of the paymentCVV
FullName | FullName on account 

##### BPay
Column | Description 
-------|------------
PaymentId | Unique Identifier 
BRoutingNumKey | Key of routing number
BRountingNumMask | Mask of routing number 
BAccountNumKey | Key of account number
BAccountNumMask | key of routing number 
FullName | FullName on account 

##### Contact 
Column | Description 
-------|-------------
ContactId | Unique Identifier 
Email | Email of member 
HomePhone | Home phone of member 
CellPhone | Cell phone of member 
AbleToText | Bool, if member wants to receive text messages 
ReceiveNotif | Bool, if member wants to receive notifications from 
Active |  Bool, if contact is active with this member
MemberId | Member id 

##### Address 
Column | Description 
-------|------------
AddressId | Unqie Identifier 
StreetNumber | Street Number 
StreetName | Street Name 
StreetType | Street Type 
AptUnit | Apt unit with room number 
City    | City of where member lives 
State | State of where member lives 
ZipCode | ZipCode of where member lives
MemberId | Member id

##### Reference 
Column | Description 
-------|------------
ReferenceID | Unique Identifier 
Relationship | Relationship to member  
FirstName   | First name of reference 
LastName    | Last ane of reference 
PhoneNumber | Phone number 
MemberId | Foreign key 

##### Enrollment
Enrollment with a provider 

Column | Description 
-------|--------------
EnrollId | unique
StartDate | The date that the member enrolled with the smale provider 
TermDate | The termdate that the member will no long with be with provider 
Active |  bool, if member is with provider yes or no
InsuranceInfo | bool, if member provided insurance information 
MedicalHistInfo | bool, if member provider medical history 
ReferenceInfo | bool, if member provided references at enrollement 



