# Member Web Service Project Structure 
The member web service will be built on .NETCore3.0 the latest of .NETCore. The development methodology we willl use is TDD (Test-Drive Development) and with the other various design patterns. We will adapt the bottom to top development approach as well, meaning that 
we are going to start with the creation of relational objects/entities and pushing them to SQL Server via the Entity Framework. Overall, this webservice will be develop by using the .NET Core CLI for better hands-on configuration and knowledge on how to. 

![Image of Project Structure](Images/Project_Structure.png "Project Structure")

Core Projects | Type | Description 
--------------|------|------------
WS.MemberSerivce.Solution| Solution | The solution file for this application
WS.MemberService | WebApi | ASP .NET Core WEb API C#:<br>+ Use Azure Active Directory[https://docs.microsoft.com/en-us/azure/active-directory/develop/authentication-vs-authorization]
WS.MS.Layout.XUnit | Xunit | Project testing WS.MS.Layout 
WS.MS.Layout | BusinessLayer Class Library | Project help library to group core functionlity to use in the controller.  
WS.MS.Core.XUnit | Xunit | Project for testing core functionality 
WS.MS.Core   | LogicLayer Class Library | Project containing core functionality 
WS.MS.Data.XUnit | Xunit | Project testing core data fetching functionality 
WS.MS.DataCenter | DataLayer Class Library | Entity Framework project connecting to a SQL-Server
WS.MS.DataCore | DataLayer Class library | Project on creating the OOP relational data tables 

# Member Web Service - Project Dependencies 

The architecture of this web service is monolithic not mvc. 

ws.ms.datacenter | Dependencies 
-----------------|---------------
Entity Framework | [Micrsoft.EntityFrameworkCore.SqlServer](https://docs.microsoft.com/en-us/ef/core/get-started/?tabs=netcore-cli)<br> **Install Entity Framework Core** <br> .NET Core CLI command <br>```dotnet add package Microsoft.EntityFrameworkCore.SqlServer --version 3.1.4```<br> [Database Provider](https://docs.microsoft.com/en-us/ef/core/providers/?tabs=dotnet-core-cli) <br> DbContext will be form here in this datacenter class library. <br> **Saving Connection String** <br> In ASP.NET Core the configuration system is very flexible, and the connection string could be stored in ```appsettings.json```, an environment variable, the user secret store, or another configuraiton source. - Microsoft Docs <br>**Options**<br>+ Environment variables are generally stored in plan, unencryted text. If the machine or process is compromised, environment variables can be accessed by untrusted parties. Additional measures to prevent disclosure of user se<br>+ The Secret Manager tool doesn't encrypt the stored secrets and shouldn't be treated as a trusted stored. It's for development purposes only. The keys and values are stored in a JSON configuration file in the user profile directory. <br>**Best option**<br> Environment Variables on Microsoft doc with [Azure App Service](https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/azure-apps/?view=aspnetcore-3.1&tabs=visual-studio#override-app-configuration-using-the-azure-portal) <br>**Reference** <br> + Use [multiple environments in ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/environments?view=aspnetcore-3.1)

Projects |  Dependencies 
-----------|---------------
ws.ms.core |  ws.ms.datacenter project with all of the dataccess upload and download. 
ws.ms.layout | ws.ms.core
ws.ms.memberservice  | ws.ms.layout



