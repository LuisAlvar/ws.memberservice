# Setup of Project Space: WS_DASpace 
List of commands | Purpose 
-----------------|------------
```dotnet new sln --name WS.MemberService``` | Creation of solution file 
```dotnet new classlib --name ws.ms.datacenter --framework netcoreapp3.0``` | Creation of datacenter classlib this will contain the dbcontext of entity framework
```dottnet new claslib --name ws.ms.datacore --framewrok netwcoreapp3.0``` | Creation of datacore classlib this will contain the objects or entity. 
```dotnet new xunit --name ws.ms.datacenter.xunit`` |  Creation of the xunit project of the main datacenter classlib 
```dotnetw sln ../WS.MemberSerivce.sln add **/*.csproj`` | adding all projects at the WS_DASpace level to the main solution file. 
